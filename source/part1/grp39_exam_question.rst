.. _part1:


*************************************************************************************************
Part 1 | Lexical Analysis
*************************************************************************************************

'NFA and DFA' proposed by Group 39, Antoine Popeler & Simon Hick
==========================================================


What are the differences between a NFA and a DFA?
""""""""""""""
Answer:

* There is no ε moves in DFA.

* There is at most one move from any state given a single input symbol a.

Why do we use NFAs?
""""""""""""""
Answer:

The only interest in NFA is the fact that they are an intermediate step from regexp to DFA.

Transform this NFA into a DFA that accepts exactly the same language.
""""""""""""""
.. image:: NFA.svg
  :alt: NFA

Answer (DFA):

.. image:: DFA.svg
  :alt: DFA

Minimise the DFA you obtained.
""""""""""""""
Answer (DFA minimised):

.. image:: DFA_mini.svg
  :alt: DFA_mini

Give a regular expression describing the language accepted by the NFA, knowing that a? means zero or one occurrence of a.
""""""""""""""
Answer:

:math:`( a (a^+ b)^* b a^* )?`
