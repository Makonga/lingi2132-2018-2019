.. _part2:

*************************************************************************************************
Partie 2 | Parsing
*************************************************************************************************

Question 1 :
------------

Give the left recursion removal algorithm. Use this algorithm to remove recursion in the following grammar :

1. S := Aa | b
2. A := Bc | e
3. B := Se | d

Question 2 :
------------

Write an unambiguous grammar for arithmetic expression.

Question Title proposed by Group 43, Minet Jeremy and Charlier Gilles
=====================================================================

Grammar :
---------

1. E ::= TQ
2. Q ::= +TQ
3. Q ::= -TQ
4. Q ::= ε
5. T ::= FR
6. R ::= \*FR
7. R ::= /FR
8. R ::= ε
9. F ::= (E)
10. F ::= id

Steps of derivation :
---------------------

+-----------------------------------+-----------------------------------+-----------------------------------+
| Stack                             | Input                             | Output                            |
+===================================+===================================+===================================+
| #E                                | *id*/(id-id)#                     |                                   |
+-----------------------------------+-----------------------------------+-----------------------------------+
| #QT                               | *id*/(id-id)#                     | 1                                 |
+-----------------------------------+-----------------------------------+-----------------------------------+
| \.\.\.                            | \.\.\.                            | \.\.\.                            |
+-----------------------------------+-----------------------------------+-----------------------------------+

Questions :
-----------

Given the above grammar, answer the following questions :

1. What should be the result of **table[R,)]** ?
2. How many entries of the corresponding table should be filled ?
3. How many times is the 8th rule applied ?
4. Compute the first set of T.
5. Given the input **id/(id - id)**, mimic the behaviour of the LL(1) parser by completing the above top-down derivation.

Hint :
------

As you may have figured out, building the corresponding table of this grammar will save you a lot of time.



CFG Disambiguation proposed by Group 14, Beznik Thomas and Delaunoy Valentin
==========================================================

Given the following grammar:

* S -> AA
* A -> SS | a

1. Show that this grammar is ambiguous
""""""""""""""""""""""""""""""""""""""

2. Explain the language described by the grammar
""""""""""""""""""""""""""""""""""""""""""""""""

3. Disambiguate the grammar
"""""""""""""""""""""""""""

Answers
"""""""

1.
""

Diagram 1:
	.. image:: img/14-diagram1.png

2.
""
	.. math::
		a^(2+3n)
3.
""

* S -> aaA
* A -> aaaA | epsilon
